Package.describe({
  name: 'aldricus:widgets',
  version: '0.0.1',
  summary: 'Simple form widgets',
  git: 'https://bitbucket.org/aldricus/meteor-widgets',
  documentation: ''
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.2');
  api.use('templating'); // Required to load template
  api.addFiles([
    'fileinputs.html',
    'widgets.html',
    'cordova.js',
    'widgets.js',
    'fileinputs.js',
    'widgets.css'
    ], ['client']);
  api.addAssets([
    'imgs/clear25.png',
    'imgs/add50.png',
    'imgs/login.png',
    'imgs/logoff.png',
    'imgs/add50-red.png',
    'imgs/check50.png',
    'imgs/check50-green.png',
    'imgs/camera50.png',
    'imgs/filemanager.png',
    'imgs/load.gif'
    ], ['client']);
  api.addFiles([
    'clientSearchEngine.css',
    'clientSearchEngine.html',
    'clientSearchEngine.js'],
    ['client']);
  api.addAssets([
    'imgs/search.png'
    ],
    ['client']);
  api.export('ldrcWidgets');
  api.export('CSE');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('aldricus:widgets');
  api.addFiles('aldricus:widgets-tests.js');
});
