/*
    Copyright (C) 2015  Aldric Thomazo
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';
/*
	Configuration object
 */
CSE = {
    /*
	User defined  parameters and callback:
	fields: {}
	template: function(){data, i}
	onSelectItem: function(item, oField, index){}
	 */
    defaultHeight: 30
};

/*
	Private function
 */
var skip = 0;
var createPlaceholder = function(skip) {
    skip = (skip) ? skip : 0;
    var placeholder = '',
        i = 0;
    _.each(CSE.fields, function(value, key, list) {
        if ((skip - 1) < i) {
            placeholder += value + ' > ';
        }
        i++;
    });
    return placeholder;
};

var initFieldsValue = function() {
    var fieldsValue = {},
        i = 0;
    _.each(CSE.fields, function(value, key, list) {
        var tmp = {};
        tmp.value = '';
        tmp.key = key;
        fieldsValue[i] = tmp;
        i++;
    });
    CSE.fieldsValue = fieldsValue;
};

var showPlaceHolder = function(template, iSkip, callback) {
    var placeholder = createPlaceholder(iSkip);
    template.$('.cse-placeholder').css('display', 'block').text(placeholder)
        .width(template.$('.cse-value-container').width() - 30 - 31);

    CSE.$input.addClass('placeholder').val('');
    return template.$('.cse-placeholder').height();
};
/*
	Meteor Template methods
 */
Template.clientSearchEngine.onCreated(function() {

});

Template.clientSearchEngine.onRendered(function() {

    if (CSE.fields === undefined) {
        throw new Error('Fields is empty');
    }
    var that = this;
    initFieldsValue();
    CSE.$input = that.$('.cse-input');
    console.log(1);

    // Init placeholder and return its height
    var placeHolderHeight = showPlaceHolder(that, 0);

    console.log(placeHolderHeight)

    if (placeHolderHeight < 30) {
        placeHolderHeight = 30; // min height
    }

    that.$('.cse-text-zone').height(placeHolderHeight + 1);

    that.$('.cse-value-container').css('top', that.$('.cse-text-zone')
        .height() + 'px');

    that.$('.cse-text-zone').width(that.$('.cse-value-container').innerWidth());

    // Init autocomplete
    var autocomplete = new AutoComplete('.cse-text-zone');
    autocomplete.init({
        termLength: 1,
        source: function(term, responseCallback) {

            // User need to declare this function in order
            // to inject the search result (Collection, string, etc)
            // to the Autocomplete widget
            CSE.dataSource = function(data) {
                responseCallback(data);
            };
            // trigger event on input in order to give
            // a chance to lauch a db search on input change
            CSE.$input.trigger('cse-search-' + that.data.id +
                '-event', [term, CSE.fieldsValue[skip]]
            );
        },
        template: function(data, i) {

            // user defined template
            if (CSE.template) {
                return CSE.template(data, i, CSE.fieldsValue[
                    skip]);
            } else {
                throw new Error(
                    'You must define a list Template callback'
                );
            }
        },
        clickItem: function(el, item) {

            if (CSE.onSelectItem) {
                var firstItemClass = '';
                if (skip < _.size(CSE.fieldsValue)) {
                    if (skip == 1) {
                        firstItemClass = 'first-item';
                    }
                    that.$('.cse-value-container').css(
                        'visibility', 'visible');
                    that.$('.cse-placeholder')
                        .css('display', 'block')
                        .text(createPlaceholder(++skip));
                    CSE.$input.val('').focus();
                    CSE.fieldsValue[skip - 1].value = $(
                        item);
                    that.$('.cse-list-item').find(
                        '.cse-remove').removeClass(
                        'cse-remove').html('');
                    that.$('.cse-list-item').find('li').removeClass(
                        'last-value');
                    that.$('.cse-list-item').append(
                        '<li class="last-value cse-value ' +
                        firstItemClass + '">' +
                        CSE.onSelectItem(item, CSE.fieldsValue[
                            skip - 1], skip - 1) +
                        '<span class="cse-remove cse-remove-span"></span></li>'
                    );
                    //CSE.$input.css('height',  h + 'px');
                    var w = that.$('.cse-value-container').width() -
                        50;
                    that.$('.cse-first-item').css(
                        'max-width', w + 'px');
                }
            } else {
                throw new Error(
                    'You must define a select item callback'
                );
            }
        },
        onLoaded: function() {
            // Hack to properly aligne AC once the autocomplete is loaded
            //console.log(that.$('.ldrc-autocomplete'));
            that.$('.ldrc-autocomplete').css('top', that.$(
                '.cse-text-zone').height() + 'px');
        }
    });

    // For small screen
    /*if(that.$(window).width() <= 680){ // raise an error!!!
		//that.$('.cse-clt-slide').toggle();
		//that.$('.cse-clt-slide').addClass('clt-small');
			console.log(7);

	}*/

});

Template.clientSearchEngine.events({
    'click .cse-placeholder, touchend .cse-placeholder, keydown input, keydown input': function(
        event, template) {
        template.$('.cse-placeholder').hide();
        template.$('.cse-input').focus();
    },
    'blur .cse-input': function(event, template) {
        template.$('.cse-placeholder').show();
        template.$('.cse-input').val('');
        template.$('.cse-text-zone').removeClass('cse-outline');
    },
    'click .cse-remove, touchend .cse-remove': function(event, template) {
        event.preventDefault();
        template.$('.cse-list-item').find('li:last').remove();
        template.$('.cse-list-item').find('li:last').addClass(
            'last-value');
        template.$('.cse-list-item').find(
            'li:last .cse-remove-span').addClass('cse-remove');
        showPlaceHolder(template, --skip);
    },
    'focus .cse-input': function(event, template) {
        template.$('.cse-text-zone').addClass('cse-outline');
    },
    'click .clear-text, touchend .clear-text': function(event, template) {
        event.preventDefault();
        showPlaceHolder(template, skip);
    }
});