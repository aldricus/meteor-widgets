##  Simple form widgets for Meteor Template ##


### Examples: ###

     Read field: {{ >ldrcInput type="text" id="type" label="My Label:" value="myValue" edit=false}}

     Edit field:   {{ >ldrcInput  type="text" id="myId" label="My Label:"}}

     Mandatory field:
     {{ >ldrcInput  type="text" id="myId" label="My Label:" mandatoryMsg="This field cannot be empty" }}