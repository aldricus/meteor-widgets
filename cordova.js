Meteor.startup(function() {
    Cordova = {};
    /*
		Camera and file browser
	 */
    Cordova.getPhotoWithSize = function(source, onSuccess) {
        // Retrieve image file location from specified source
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            targetWidth: 960,
            //targetHeight: 960,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: source
        });
    };

    /*function onSuccess(imageData) {
        var image = document.getElementById('myImage');
        image.src = "data:image/jpeg;base64," + imageData;
        	console.log('imageData: ');
        	console.log(imageData);
        	callback(imageData);
    	};*/

    function onFail(err) {
        console.log(err);
    };


    /*
    	Notification
     */

    // Amazon Fire OS / Android / BlackBerry 10 (OS 5.0 and higher) / iOS / Tizen
    //
    function alertDismissed() {
        // do something
        console.log('alertDismissed');
    };

    Cordova.alert = function() {
        console.log('alert');
        navigator.notification.alert(
            'You are the winner!', // message
            alertDismissed, // callback
            'Game Over', // title
            'Done' // buttonName
        )
    }

});

/*
	Camera.PictureSourceType = {
	    PHOTOLIBRARY : 0,
	    CAMERA : 1,
	    SAVEDPHOTOALBUM : 2
	};
 */