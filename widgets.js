/*
    Meteor Simple  widgets - Copyright (C) 2015  Aldric T
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
	Local functions
 */
ldrcWidgets = {};
var hasLabel = function() {
    var edit = false;
    if (this.edit !== undefined && this.label != undefined) {
        edit = true;
    }
    return ((this.label !== undefined || this.i18n !== undefined) || edit);
};
/*
 Input Helpers
 */
Template.ldrcInput.onCreated(function() {
    this.data.inputIsFocused = false;
    this.data.clearOver = false;
})
Template.ldrcInput.onRendered(function() {
    var $this = this;
    $('form').submit(function(e) {
        var reqInput = $(this).find($($this.find(
            'input,textarea,select')));
        if (reqInput.attr('data-required') && reqInput.val().trim() ===
            "") {
            $($this.find('.mandatory-msg')).css('visibility',
                'visible');
            reqInput.addClass('required-input').focus();
            return false;
        }
        return true;
    });

});

Template.ldrcInput.helpers({
    isTextarea: function() {
        return this.type == 'textarea';
    },
    required: function() {
        return this.mandatoryMsg !== undefined;
    },
    hasLabel: hasLabel,
    edit: function() {
        if (this.edit == undefined || this.edit) {
            return true;
        } else {
            return false;
        }
    },
    name: function() {
        return this.name === undefined ? this.id : this.name;
    },
    cid: function() {
        return (this.id !== undefined) ? this.id : this.name;
    },
    txtId: function() {
        return (this.id !== undefined) ? {
            id: this.id
        } : "";
    },
    label: function() {
        var label = this.label;
        if (this.i18n) {
            label = i18n(this.i18n);
        }
        return label;
    }
})
Template.ldrcInput.events({
    'mouseover': function(event, template) {
        if ($(template.find('input')).val() !== '' && !template.data
            .inputIsFocused) {
            $(template.find('span.clear')).css('visibility',
                'visible');
        }
    },
    'mouseover  span.clear': function(event, template) {
        template.data.clearOver = true;
        $(template.find('input')).trigger('blur'); // Preventing the need to click twice to fire event
    },
    'mouseout  span.clear': function(event, template) {
        template.data.clearOver = false;
        if ($(template.find('input')).val() === '') {
            $(template.find('span.clear')).css('visibility',
                'hidden');
        }
    },
    'mouseout ': function(event, template) {
        if ($(template.find('input')).val() !== '' && !template.data
            .inputIsFocused) {
            $(template.find('span.clear')).css('visibility',
                'hidden');
        }
    },
    'focus input': function(event, template) {
        event.preventDefault();
        $(template.find('span.clear')).css('visibility', 'visible');
        template.data.inputIsFocused = true;
    },
    'blur input': function(event, template) {
        if (!template.data.clearOver) {
            $(template.find('span.clear')).css('visibility',
                'hidden');
        }
        template.data.inputIsFocused = false;
    },
    'click  span.clear, touchstart span.clear': function(event,
        template) {
        event.preventDefault();
        $(template.find('input, textarea')).val('').trigger(
            'change');
        $(template.find('input, textarea')).focus();
        //$(template.find('span.clear')).css('visibility', 'hidden');
    },
    'change input, change select, change textarea': function(event,
        template) {
        $(template.find('.mandatory-msg')).css('visibility',
            'hidden');
        $(template.find('input')).removeClass('required-input');
    },
    'touchend span.clear': function(event, template) {
        event.preventDefault();
        $(template.find('input')).trigger('blur');
    }
});
/*
	Sooring Widgets
 */
Template.ldrcScoring.helpers({
    imgs: function() {
        if (this.score > 0) {
            var htmlImgs = '',
                nb = 0,
                mod = 0,
                nbImgs = 0,
                i = 0;
            var style =
                'width: 17px; height: 18px; float: left; display: inline;  background:  url(' +
                this.sprite + ')';
            var posFull = style + ' -17px 0';
            var posNone = style + ' 0 0';
            var posHalf = style + ' -34px 0';
            if (this.maxScore == undefined) {
                this.maxScore = 5;
            }
            //compute score
            nbImgs = Math.round(this.score / 0.5);
            mod = nbImgs % 2;
            // Display one score point
            for (i = 0; i < (nbImgs - mod) / 2; i++) {

                htmlImgs += '<div  style="' + posFull + '"></div>';
                nb++;
            };
            // Display half of one score point
            if (mod > 0) {
                htmlImgs += '<div style="' + posHalf + '"></div>';
                nb++;
            }
            // Display empty point
            for (i = 0; i < this.maxScore - nb; i++) {
                htmlImgs += '<div style="' + posNone + '"></div>';
            };
            return new Handlebars.SafeString(htmlImgs);
        } else {
            //return new Handlebars.SafeString("<span>Not Rated</span>");
            return '';
        }
    },
    hasLabel: hasLabel
});

ldrcWidgets.score = 0;

Template.ldrcRateThis.rendered = function() {
    Session.set('ldrcWidgetScore', ldrcWidgets.score);
};

Template.ldrcRateThis.helpers({
    imgs: function() {
        var htmlImgs = '',
            nb = 0,
            mod = 0,
            nbImgs = 0,
            i = 0;
        // Todo: image plus grande !!!
        var style =
            'width: 30px; height: 30px; float: left; display: inline;  background:  url(' +
            this.sprite + ')';
        var posFull = style + ' -30px 0';
        var posNone = style + ' 0 0';
        var posHalf = style + ' -30px 0';

        // Display empty point
        for (i = 0; i < 5; i++) {
            htmlImgs += '<div class="rate notrated" style="' +
                posNone + '"></div>';
        };
        return new Handlebars.SafeString(htmlImgs);
    },
    hasLabel: hasLabel,
    rateScore: function() {
        return Session.get('ldrcWidgetScore');
    }
});
Template.ldrcRateThis.events({
    'click .rate, touchleave .rate': function(event, template) {
        event.preventDefault();
        var rateDivObj = $(event.target);
        if (rateDivObj.hasClass('notrated')) {
            rateDivObj.css('background', 'url(' + this.sprite +
                ') -30px 0');
            rateDivObj.addClass('rated');
            rateDivObj.removeClass('notrated');
            ldrcWidgets.score++;
        } else {
            rateDivObj.css('background', 'url(' + this.sprite +
                ') 0 0');
            rateDivObj.addClass('notrated');
            rateDivObj.removeClass('rated');
            ldrcWidgets.score--
        }
        Session.set('ldrcWidgetScore', ldrcWidgets.score);
    },
    /*'mouseout, touchend': function(event, template){
		//TODO
	}*/
});
/*
	Buttons Widgets
 */
var src = '/packages/aldricus_widgets/imgs/';
Template.ldrcAddBtn.events({
    'mouseover, touchstart': function(event, template) {
        event.preventDefault();
        $(template.find('input')).attr('src', src + 'add50-red.png');
    },
    'mouseout, touchend': function(event, template) {
        event.preventDefault();
        $(template.find('input')).attr('src', src + 'add50.png');
    }
});
Template.ldrcCheckBtn.events({
    'mouseover, touchstart': function(event, template) {
        event.preventDefault();
        $(template.find('input')).attr('src', src +
            'check50-green.png');
    },
    'mouseout, touchend': function(event, template) {
        event.preventDefault();
        $(template.find('input')).attr('src', src + 'check50.png');
    }
});